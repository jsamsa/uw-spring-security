/**
 * 
 */
package edu.wisc.uwss.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * {@link HttpSecurityAmender} that marks all URLs as requiring authentication.
 * 
 * @author Nicholas Blair
 */
public class EverythingRequiresAuthenticationHttpSecurityAmender implements HttpSecurityAmender {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  /**
   * {@inheritDoc}
   */
  @Override
  public void amend(HttpSecurity http) throws Exception {
    logger.info("all urls require authentication");
    http
      .authorizeRequests()
        .antMatchers("/**").authenticated();
  }

}
