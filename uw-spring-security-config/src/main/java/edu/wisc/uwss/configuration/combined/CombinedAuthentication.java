/**
 * 
 */
package edu.wisc.uwss.configuration.combined;

import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * This class is primarily intended to help document the use case for activating both "local-users"
 * and "preauth" {@link Profile}s at the same time.
 * 
 * One of the trickiest parts of getting the *WebSecurityConfiguration classes in the 2 packages to
 * initialize correctly is their use of the {@link Order} annotation. Both of those classes extend 
 * {@link WebSecurityConfigurerAdapter}, which sets an {@link Order} of 100 by default.
 * We want the "preauth" WebSecurityConfiguration class to load first (hence {@link #PREAUTH_WEB_SECURITY_CONFIGURATION_ORDER}
 * being 99), and the "local-users" WebSecurityConfiguration class to load second ({@link #LOCAL_USERS_WEB_SECURITY_CONFIGURATION_ORDER}.
 * 
 * @author Nicholas Blair
 */
public final class CombinedAuthentication {

  /**
   * 
   */
  public static final int PREAUTH_WEB_SECURITY_CONFIGURATION_ORDER = 99;
  /**
   * 
   */
  public static final int LOCAL_USERS_WEB_SECURITY_CONFIGURATION_ORDER = 101;
  
  
}
