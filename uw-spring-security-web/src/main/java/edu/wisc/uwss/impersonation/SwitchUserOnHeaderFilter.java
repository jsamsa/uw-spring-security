/**
 * 
 */
package edu.wisc.uwss.impersonation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.switchuser.AuthenticationSwitchUserEvent;
import org.springframework.security.web.authentication.switchuser.SwitchUserAuthorityChanger;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;
import org.springframework.security.web.authentication.switchuser.SwitchUserGrantedAuthority;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.web.filter.GenericFilterBean;

/**
 * Extension of {@link SwitchUserFilter} that allows for user switching based on a 
 * configurable HTTP header.
 * 
 * The basic {@link SwitchUserFilter} is typically applied around a custom URL with something like the following:
 * <pre>
 http.authorizeRequests()
     .antMatchers("/j_spring_security_switch_user").hasRole("ROLE_SUPERVISOR");
 </pre>
 * or
 * <pre>
   <http>
   ...
   <custom-filter position="SWITCH_USER_FILTER" ref="switchUserProcessingFilter" />
   <intercept-url pattern="/j_spring_security_switch_user" access="hasRole('ROLE_SUPERVISOR')"/>
 </pre>
 * 
 * <strong>This filter is configured differently.</strong> {@link #requiresSwitchUser(HttpServletRequest)} and
 * {@link #requiresExitUser(HttpServletRequest)} now perform the authorization test implicitly; returning false if the 
 * user is not authorized to attempt switching. Simply register an instance of this filter, make sure to provide
 * it with your UserDetailsService, set your desired header names and required {@link GrantedAuthority}.
 * 
 * @author Nicholas Blair
 */
public class SwitchUserOnHeaderFilter extends GenericFilterBean {

  /**
   * Default HTTP Header used to trigger {@link #attemptSwitchUser(HttpServletRequest)}.
   */
  public static final String DEFAULT_SWITCH_USER_HEADER_NAME = "On-Behalf-Of";
  /**
   * Default HTTP Header used to trigger {@link #attemptExitUser(HttpServletRequest)}.
   */
  public static final String DEFAULT_SWITCH_EXIT_HEADER_NAME = "Switch-Exit";
  /**
   * Default {@link GrantedAuthority} required to switch user.
   */
  public static final String DEFAULT_SWITCH_USER_REQUIRED_GRANTED_AUTHORITY = "ROLE_ADMIN";
  
  public static final String ROLE_PREVIOUS_ADMINISTRATOR = "ROLE_PREVIOUS_ADMINISTRATOR";
  private static final Logger logger = LoggerFactory.getLogger(SwitchUserOnHeaderFilter.class);
  private String switchUserHeaderName = DEFAULT_SWITCH_USER_HEADER_NAME;
  private String switchExitHeaderName = DEFAULT_SWITCH_EXIT_HEADER_NAME;
  private String requiredGrantedAuthority = DEFAULT_SWITCH_USER_REQUIRED_GRANTED_AUTHORITY;

  private ApplicationEventPublisher eventPublisher;
  private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();
  protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();
  private UserDetailsService userDetailsService;
  private UserDetailsChecker userDetailsChecker = new AccountStatusUserDetailsChecker();
  private AuthenticationSuccessHandler successHandler = new SavedRequestAwareAuthenticationSuccessHandler();
  private String switchFailureUrl;
  private AuthenticationFailureHandler failureHandler;
  private SwitchUserAuthorityChanger switchUserAuthorityChanger;
  /**
   * @return the switchUserHeaderName
   */
  public String getSwitchUserHeaderName() {
    return switchUserHeaderName;
  }
  /**
   * @param switchUserHeaderName the switchUserHeaderName to set
   */
  public void setSwitchUserHeaderName(String switchUserHeaderName) {
    this.switchUserHeaderName = switchUserHeaderName;
  }
  /**
   * @return the switchExitHeaderName
   */
  public String getSwitchExitHeaderName() {
    return switchExitHeaderName;
  }
  /**
   * @param switchExitHeaderName the switchExitHeaderName to set
   */
  public void setSwitchExitHeaderName(String switchExitHeaderName) {
    this.switchExitHeaderName = switchExitHeaderName;
  }
  /**
   * @return the requiredGrantedAuthority
   */
  public String getRequiredGrantedAuthority() {
    return requiredGrantedAuthority;
  }
  /**
   * @param requiredGrantedAuthority the requiredGrantedAuthority to set
   */
  public void setRequiredGrantedAuthority(String requiredGrantedAuthority) {
    this.requiredGrantedAuthority = requiredGrantedAuthority;
  }
  /**
   * @return the switchFailureUrl
   */
  public String getSwitchFailureUrl() {
    return switchFailureUrl;
  }
  /**
   * @param switchFailureUrl the switchFailureUrl to set
   */
  public void setSwitchFailureUrl(String switchFailureUrl) {
    this.switchFailureUrl = switchFailureUrl;
  }
  /**
   * @return the eventPublisher
   */
  public ApplicationEventPublisher getEventPublisher() {
    return eventPublisher;
  }
  /**
   * @param eventPublisher the eventPublisher to set
   */
  @Autowired
  public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
    this.eventPublisher = eventPublisher;
  }
  /**
   * @return the authenticationDetailsSource
   */
  public AuthenticationDetailsSource<HttpServletRequest, ?> getAuthenticationDetailsSource() {
    return authenticationDetailsSource;
  }
  /**
   * @param authenticationDetailsSource the authenticationDetailsSource to set
   */
  @Autowired(required=false)
  public void setAuthenticationDetailsSource(
      AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource) {
    this.authenticationDetailsSource = authenticationDetailsSource;
  }
  /**
   * @return the userDetailsService
   */
  public UserDetailsService getUserDetailsService() {
    return userDetailsService;
  }
  /**
   * @param userDetailsService the userDetailsService to set
   */
  @Autowired
  public void setUserDetailsService(UserDetailsService userDetailsService) {
    this.userDetailsService = userDetailsService;
  }
  /**
   * @return the userDetailsChecker
   */
  public UserDetailsChecker getUserDetailsChecker() {
    return userDetailsChecker;
  }
  /**
   * @param userDetailsChecker the userDetailsChecker to set
   */
  public void setUserDetailsChecker(UserDetailsChecker userDetailsChecker) {
    this.userDetailsChecker = userDetailsChecker;
  }
  /**
   * @return the successHandler
   */
  public AuthenticationSuccessHandler getSuccessHandler() {
    return successHandler;
  }
  /**
   * @param successHandler the successHandler to set
   */
  public void setSuccessHandler(AuthenticationSuccessHandler successHandler) {
    this.successHandler = successHandler;
  }
  /**
   * @return the failureHandler
   */
  public AuthenticationFailureHandler getFailureHandler() {
    return failureHandler;
  }
  /**
   * @param failureHandler the failureHandler to set
   */
  public void setFailureHandler(AuthenticationFailureHandler failureHandler) {
    this.failureHandler = failureHandler;
  }
  /**
   * @return the switchUserAuthorityChanger
   */
  public SwitchUserAuthorityChanger getSwitchUserAuthorityChanger() {
    return switchUserAuthorityChanger;
  }
  /**
   * @param switchUserAuthorityChanger the switchUserAuthorityChanger to set
   */
  public void setSwitchUserAuthorityChanger(SwitchUserAuthorityChanger switchUserAuthorityChanger) {
    this.switchUserAuthorityChanger = switchUserAuthorityChanger;
  }
  /* (non-Javadoc)
   * @see org.springframework.security.web.authentication.switchuser.SwitchUserFilter#afterPropertiesSet()
   */
  @Override
  public void afterPropertiesSet() {
    if (failureHandler == null) {
      failureHandler = switchFailureUrl == null ? new SimpleUrlAuthenticationFailureHandler()
          : new SimpleUrlAuthenticationFailureHandler(switchFailureUrl);
    } else {
      Assert.isNull(switchFailureUrl,
          "You cannot set both a switchFailureUrl and a failureHandler");
    }
  }

  /**
   * 
   */
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;

    // check for switch or exit request
    if (requiresSwitchUser(request)) {
      // if set, attempt switch and store original
      try {
        Authentication targetUser = attemptSwitchUser(request);

        // update the current context to the new target user
        SecurityContextHolder.getContext().setAuthentication(targetUser);

      }
      catch (AuthenticationException e) {
        logger.debug("Switch User failed", e);
        failureHandler.onAuthenticationFailure(request, response, e);
      }

    }
    else if (requiresExitUser(request)) {
      //TODO not sure if this block is neccessary as we don't have a use case for users exiting a swich.
      
      // get the original authentication object (if exists)
      Authentication originalUser = attemptExitUser(request);

      // update the current context back to the original user
      SecurityContextHolder.getContext().setAuthentication(originalUser);

      // redirect to target url
      successHandler.onAuthenticationSuccess(request, response, originalUser);

    }

    chain.doFilter(request, response);
  }
  
  /**
   * @param authentication
   * @return true only if the {@link Authentication} contains a {@link GrantedAuthority} where
   *         {@link GrantedAuthority#getAuthority()} equals {@link #getRequiredGrantedAuthority()}.
   */
  protected boolean matchesRequiredGrantedAuthority(Authentication authentication) {
    boolean result = false;
    if(authentication != null) {
      for (GrantedAuthority authority : authentication.getAuthorities()) {
        if (authority.getAuthority().equals(getRequiredGrantedAuthority())) {
          result = true;
        }
      }
    }
    logger.debug("matchesGrantedAuthority for authentication {}, looking for {}, returning {}", authentication, getRequiredGrantedAuthority(), result);
    return result;
  }
  
  /**
   * 
   * @param request
   * @return true if the {@link HttpServletRequest} indicates we should attempt to switch users
   */
  protected boolean requiresSwitchUser(HttpServletRequest request) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if(matchesRequiredGrantedAuthority(authentication)){
      String onBelhalfOfUsername = request.getHeader(getSwitchUserHeaderName());
      boolean result = StringUtils.isNotBlank(onBelhalfOfUsername);
      if(result) {
        logger.debug("authorized use of switch user header {} by authentication {}", getSwitchUserHeaderName(), authentication);
      }
      return result;
    }
    return false;
  }
  /**
   * 
   * @param request
   * @return true if the {@link HttpServletRequest} indicates we should attempt to exit from a switched user
   */
  protected boolean requiresExitUser(HttpServletRequest request) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if(matchesRequiredGrantedAuthority(authentication)){
      String headerValue = request.getHeader(getSwitchExitHeaderName());
      boolean result = StringUtils.isNotBlank(headerValue);
      if(result) {
        logger.debug("authorized use of switch exit header {} by authentication {}", getSwitchExitHeaderName(), authentication);
      }
      return result;
    }
    return false;
  }
  /**
   * Returns the value of {@link #getSwitchUserHeaderName()}.
   * 
   * @param request
   * @return the username that is the target of the switch user request.
   */
  protected String getSwitchTargetUsername(HttpServletRequest request) {
    return request.getHeader(getSwitchUserHeaderName());
  }
  /**
   * Attempt to switch to another user. If the user does not exist or is not active,
   * return null.
   *
   * @return The new <code>Authentication</code> request if successfully switched to
   * another user, <code>null</code> otherwise.
   *
   * @throws UsernameNotFoundException If the target user is not found.
   * @throws LockedException if the account is locked.
   * @throws DisabledException If the target user is disabled.
   * @throws AccountExpiredException If the target user account is expired.
   * @throws CredentialsExpiredException If the target user credentials are expired.
   */
  protected Authentication attemptSwitchUser(HttpServletRequest request)
      throws AuthenticationException {
    UsernamePasswordAuthenticationToken targetUserRequest;

    String username = getSwitchTargetUsername(request);

    if (username == null) {
      username = "";
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Attempt to switch to user [" + username + "]");
    }

    UserDetails targetUser = getUserDetailsService().loadUserByUsername(username);
    getUserDetailsChecker().check(targetUser);

    // OK, create the switch user token
    targetUserRequest = createSwitchUserToken(request, targetUser);

    if (logger.isDebugEnabled()) {
      logger.debug("Switch User Token [" + targetUserRequest + "]");
    }

    // publish event
    if (getEventPublisher() != null) {
      getEventPublisher().publishEvent(new AuthenticationSwitchUserEvent(
          SecurityContextHolder.getContext().getAuthentication(), targetUser));
    }

    return targetUserRequest;
  }
  /**
   * Attempt to exit from an already switched user.
   *
   * @param request The http servlet request
   *
   * @return The original <code>Authentication</code> object or <code>null</code>
   * otherwise.
   *
   * @throws AuthenticationCredentialsNotFoundException If no
   * <code>Authentication</code> associated with this request.
   */
  protected Authentication attemptExitUser(HttpServletRequest request)
          throws AuthenticationCredentialsNotFoundException {
      // need to check to see if the current user has a SwitchUserGrantedAuthority
      Authentication current = SecurityContextHolder.getContext().getAuthentication();

      if (null == current) {
          throw new AuthenticationCredentialsNotFoundException(messages.getMessage(
                  "SwitchUserFilter.noCurrentUser",
                  "No current user associated with this request"));
      }

      // check to see if the current user did actual switch to another user
      // if so, get the original source user so we can switch back
      Authentication original = getSourceAuthentication(current);

      if (original == null) {
          logger.debug("Could not find original user Authentication object!");
          throw new AuthenticationCredentialsNotFoundException(messages.getMessage(
                  "SwitchUserFilter.noOriginalAuthentication",
                  "Could not find original Authentication object"));
      }

      // get the source user details
      UserDetails originalUser = null;
      Object obj = original.getPrincipal();

      if ((obj != null) && obj instanceof UserDetails) {
          originalUser = (UserDetails) obj;
      }

      // publish event
      if (this.eventPublisher != null) {
          eventPublisher.publishEvent(new AuthenticationSwitchUserEvent(current,
                  originalUser));
      }

      return original;
  }


  /**
   * Create a switch user token that contains an additional <tt>GrantedAuthority</tt>
   * that contains the original <code>Authentication</code> object.
   *
   * @param request The http servlet request.
   * @param targetUser The target user
   *
   * @return The authentication token
   *
   * @see SwitchUserGrantedAuthority
   */
  private UsernamePasswordAuthenticationToken createSwitchUserToken(
          HttpServletRequest request, UserDetails targetUser) {

      UsernamePasswordAuthenticationToken targetUserRequest;

      // grant an additional authority that contains the original Authentication object
      // which will be used to 'exit' from the current switched user.

      Authentication currentAuth;

      try {
          // SEC-1763. Check first if we are already switched.
          currentAuth = attemptExitUser(request);
      }
      catch (AuthenticationCredentialsNotFoundException e) {
          currentAuth = SecurityContextHolder.getContext().getAuthentication();
      }

      GrantedAuthority switchAuthority = new SwitchUserGrantedAuthority(
              ROLE_PREVIOUS_ADMINISTRATOR, currentAuth);

      // get the original authorities
      Collection<? extends GrantedAuthority> orig = targetUser.getAuthorities();

      // Allow subclasses to change the authorities to be granted
      if (getSwitchUserAuthorityChanger() != null) {
          orig = getSwitchUserAuthorityChanger().modifyGrantedAuthorities(targetUser,
                  currentAuth, orig);
      }

      // add the new switch user authority
      List<GrantedAuthority> newAuths = new ArrayList<GrantedAuthority>(orig);
      newAuths.add(switchAuthority);

      // create the new authentication token
      targetUserRequest = new UsernamePasswordAuthenticationToken(targetUser,
              targetUser.getPassword(), newAuths);

      // set details
      targetUserRequest.setDetails(getAuthenticationDetailsSource().buildDetails(request));

      return targetUserRequest;
  }

  /**
   * Find the original <code>Authentication</code> object from the current user's
   * granted authorities. A successfully switched user should have a
   * <code>SwitchUserGrantedAuthority</code> that contains the original source user
   * <code>Authentication</code> object.
   *
   * @param current The current <code>Authentication</code> object
   *
   * @return The source user <code>Authentication</code> object or <code>null</code>
   * otherwise.
   */
  private Authentication getSourceAuthentication(Authentication current) {
      Authentication original = null;

      // iterate over granted authorities and find the 'switch user' authority
      Collection<? extends GrantedAuthority> authorities = current.getAuthorities();

      for (GrantedAuthority auth : authorities) {
          // check for switch user type of authority
          if (auth instanceof SwitchUserGrantedAuthority) {
              original = ((SwitchUserGrantedAuthority) auth).getSource();
              logger.debug("Found original switch user granted authority [" + original
                      + "]");
          }
      }

      return original;
  }
}
