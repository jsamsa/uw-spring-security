/**
 * 
 */
package edu.wisc.uwss.preauth;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.UWUserDetailsImpl;

/**
 * Interface providing a mechanism to the attributes preauthenticated in the
 * {@link HttpServletRequest} to a {@link UWUserDetails} instance.
 *
 * This interface is executed during authentication attempts (similar to
 * {@link edu.wisc.uwss.local.LocalUsersAuthenticationAttemptCallback}).
 * 
 * @author Nicholas Blair
 */
public interface PreauthenticatedUserDetailsAttributeMapper {

  /**
   * Construct a {@link UWUserDetails} from the attributes and headers in the {@link HttpServletRequest}.
   * 
   * @param request
   * @return a {@link UWUserDetails} instance, or null of none can be created.
   */
  UWUserDetails mapUser(HttpServletRequest request);
  
  /**
   * Default {@link PreauthenticatedUserDetailsAttributeMapper} implementation.
   * Binds well known {@link HttpServletRequest#getHeader(String)} names that are
   * exported via common Shibboleth SP configurations used here at UW-Madison.
   * 
   * @author Nicholas Blair
   */
  public static class Default implements PreauthenticatedUserDetailsAttributeMapper {

    private String eppnHeader = "eppn";
    private String pviHeader = "wiscedupvi";
    private String usernameHeader = "uid";
    private String fullNameHeader = "cn";
    private String uddsHeader = "wisceduudds";
    private String emailAddressHeader = "mail";
    private String firstNameHeader = "givenName";
    private String lastNameHeader = "sn";
    private String isisEmplidHeader = "wisceduisisemplid";
    private String identityProviderHeader = "Shib-Identity-Provider";
    private String customLogoutPrefix = "/Shibboleth.sso/Logout?return=";
    private String customLogoutSuffix = "/logout/";
    private String manifestHeader = "isMemberOf";
    
    private static final Logger logger = LoggerFactory.getLogger(Default.class);
    /**
     * {@inheritDoc}
     * 
     * Will short-circuit and return null if {@link HttpServletRequest#getHeader(String)} for {@link #getUsernameHeader()}
     * returns null.
     * Instances returned by this method will always return an empty {@link String} for {@link UWUserDetails#getPassword()}.
     */
    @Override
    public UWUserDetails mapUser(HttpServletRequest request) {
      String uid = request.getHeader(usernameHeader);
      logger.debug("enter mapUser, usernameHeader {} was {}", usernameHeader, uid);
      if(StringUtils.isBlank(uid)) {
        logger.debug("usernameHeader {} was blank, returning null", usernameHeader);
        return null;
      }
      String eppn = request.getHeader(eppnHeader);
      String pvi = request.getHeader(pviHeader);
      String cn = request.getHeader(fullNameHeader);
      String emplid = request.getHeader(isisEmplidHeader);
      Collection<String> uddsMembership = safeGetHeaders(request, uddsHeader);
      String email = request.getHeader(emailAddressHeader);

      Collection<String> manifestGroups = new ArrayList<>();
      String manifestValue = request.getHeader(manifestHeader);
      if (manifestValue != null) {
        String[] manifestGroupStrings = manifestValue.split(";");
        for (String manifestGroupString : manifestGroupStrings) {
          if (!manifestGroupString.trim().isEmpty()) {
            manifestGroups.add(manifestGroupString.trim());
          }
        }
      }

      UWUserDetailsImpl result = UWUserDetailsImpl.newInstance(pvi, uid, "", cn, email, uddsMembership, manifestGroups);
      result.setSource("edu.wisc.uwss.preauth");
      result.setEppn(eppn);
      result.setIsisEmplid(emplid);
      result.setFirstName(request.getHeader(firstNameHeader));
      result.setLastName(request.getHeader(lastNameHeader));
      String identityProvider = request.getHeader(identityProviderHeader);
      result.setCustomLogoutUrl(toCustomLogoutUrl(identityProvider));
      
      logger.debug("mapUser constructed {} from headers in request", result);
      return result;
    }

    /**
     * {@link HttpServletRequest#getHeaders(String)} has 3 possible return values:
     *
     * <ul>
     *   <li>A non-empty {@link Enumeration} of {@link String} values for the header,</li>
     *   <li>An "empty" {@link Enumeration} of {@link String} values for the header,</li>
     *   <li>null, if the container does not allow access to the header(s).</li>
     * </ul>
     *
     * Further complicating things is that some containers may return a non-empty {@link Enumeration}
     * of {@link String} values, but those {@link String} values may be blank. For this particular case,
     * {@link Collections#list(Enumeration)} is unsafe, because the returned list may include blank values.
     *
     * This method provides a safe way to get the values for the specified header as a non-null
     * (but still potentially empty) {@link List}.
     *
     * @see HttpServletRequest#getHeaders(String)
     * @see StringUtils#isNotBlank(CharSequence)
     * @param request the HTTP request
     * @param headerName the name of the multi-valued header
     * @return a never null, but potentially empty, {@link List} of not blank values for the requested header
     */
    protected List<String> safeGetHeaders(HttpServletRequest request, String headerName) {
      Enumeration<String> enumeration = request.getHeaders(headerName);
      if(enumeration == null) {
        return Collections.emptyList();
      }
      List<String> result = new ArrayList<>();
      for(String next : Collections.list(enumeration)){
        if(StringUtils.isNotBlank(next)) {
          result.add(next);
        }
      }
      return result;
    }
    /**
     * 
     * @param identityProviderHeaderValue the value of the identityProviderHeader
     * @return the corresponding log out url, or null
     */
    protected String toCustomLogoutUrl(String identityProviderHeaderValue) {
      String result = null;
      if(StringUtils.isNotBlank(identityProviderHeaderValue)) {
        try {
          URL url = new URL(identityProviderHeaderValue);
          String path = url.getPath();
          if(StringUtils.isBlank(path)) {
            result = customLogoutPrefix + identityProviderHeaderValue + customLogoutSuffix;
          } else {
            result = customLogoutPrefix + identityProviderHeaderValue.replace(url.getPath(), customLogoutSuffix);
          }
          
        } catch (MalformedURLException e) {
          logger.debug("caught MalformedURLException for identityProviderHeader={}", identityProviderHeaderValue, e);
        }
      }
      logger.debug("value of {} was {} and transformed to customLogoutUrl of {}", identityProviderHeader, identityProviderHeaderValue, result);
      return result;
    }
    /**
     * @return the eppnHeader
     */
    public String getEppnHeader() {
      return eppnHeader;
    }
    /**
     * @param eppnHeader the eppnHeader to set
     */
    @Value("${preauth.eppnHeader:eppn}")
    public void setEppnHeader(String eppnHeader) {
      this.eppnHeader = eppnHeader;
    }
    /**
     * @return the pviHeader
     */
    public String getPviHeader() {
      return pviHeader;
    }
    /**
     * @param pviHeader the pviHeader to set
     */
    @Value("${preauth.pviHeader:wiscedupvi}")
    public void setPviHeader(String pviHeader) {
      this.pviHeader = pviHeader;
    }
    /**
     * @return the usernameHeader
     */
    public String getUsernameHeader() {
      return usernameHeader;
    }
    /**
     * @param usernameHeader the usernameHeader to set
     */
    @Value("${preauth.usernameHeader:uid}")
    public void setUsernameHeader(String usernameHeader) {
      this.usernameHeader = usernameHeader;
    }
    /**
     * @return the fullNameHeader
     */
    public String getFullNameHeader() {
      return fullNameHeader;
    }
    /**
     * @param fullNameHeader the fullNameHeader to set
     */
    @Value("${preauth.fullnameHeader:cn}")
    public void setFullNameHeader(String fullNameHeader) {
      this.fullNameHeader = fullNameHeader;
    }
    /**
     * @return the uddsHeader
     */
    public String getUddsHeader() {
      return uddsHeader;
    }
    /**
     * @param uddsHeader the uddsHeader to set
     */
    @Value("${preauth.uddsHeader:wisceduudds}")
    public void setUddsHeader(String uddsHeader) {
      this.uddsHeader = uddsHeader;
    }
    /**
     * @return the emailAddressHeader
     */
    public String getEmailAddressHeader() {
      return emailAddressHeader;
    }
    /**
     * @param emailAddressHeader the emailAddressHeader to set
     */
    @Value("${preauth.emailAddressHeader:mail}")
    public void setEmailAddressHeader(String emailAddressHeader) {
      this.emailAddressHeader = emailAddressHeader;
    }
    /**
     * @return the identityProviderHeader
     */
    public String getIdentityProviderHeader() {
      return identityProviderHeader;
    }
    /**
     * @param identityProviderHeader the identityProviderHeader to set
     */
    @Value("${preauth.identityProviderHeader:Shib-Identity-Provider}")
    public void setIdentityProviderHeader(String identityProviderHeader) {
      this.identityProviderHeader = identityProviderHeader;
    }
    /**
     * @return the customLogoutPrefix
     */
    public String getCustomLogoutPrefix() {
      return customLogoutPrefix;
    }
    /**
     * @param customLogoutPrefix the customLogoutPrefix to set
     */
    @Value("${preauth.customLogoutPrefix:/Shibboleth.sso/Logout?return=}")
    public void setCustomLogoutPrefix(String customLogoutPrefix) {
      this.customLogoutPrefix = customLogoutPrefix;
    }
    /**
     * @return the customLogoutSuffix
     */
    public String getCustomLogoutSuffix() {
      return customLogoutSuffix;
    }
    /**
     * @param customLogoutSuffix the customLogoutSuffix to set
     */
    @Value("${preauth.customLogoutSuffix:/logout/}")
    public void setCustomLogoutSuffix(String customLogoutSuffix) {
      this.customLogoutSuffix = customLogoutSuffix;
    }
    /**
     * @return the isisEmplidHeader
     */
    public String getIsisEmplidHeader() {
      return isisEmplidHeader;
    }
    /**
     * @param isisEmplidHeader the isisEmplidHeader to set
     */
    @Value("${preauth.isisEmplidHeader:wisceduisisemplid}")
    public void setIsisEmplidHeader(String isisEmplidHeader) {
      this.isisEmplidHeader = isisEmplidHeader;
    }
    /**
     * @return the firstNameHeader
     */
    public String getFirstNameHeader() {
      return firstNameHeader;
    }
    /**
     * @param firstNameHeader the firstNameHeader to set
     */
    @Value("${preauth.isisEmplidHeader:givenName}")
    public void setFirstNameHeader(String firstNameHeader) {
      this.firstNameHeader = firstNameHeader;
    }
    /**
     * @return the lastNameHeader
     */
    public String getLastNameHeader() {
      return lastNameHeader;
    }
    /**
     * @param lastNameHeader the lastNameHeader to set
     */
    @Value("${preauth.isisEmplidHeader:sn}")
    public void setLastNameHeader(String lastNameHeader) {
      this.lastNameHeader = lastNameHeader;
    }
    /**
     * @return the manifestHeader
     */
    public String getManifestHeader() {
      return manifestHeader;
    }
    /**
     * @param manifestHeader the manifestHeader to set
     */
    @Value("${preauth.manifestHeader:isMemberOf}")
    public void setManifestHeader(String manifestHeader) {
      this.manifestHeader = manifestHeader;
    }
  }
}
