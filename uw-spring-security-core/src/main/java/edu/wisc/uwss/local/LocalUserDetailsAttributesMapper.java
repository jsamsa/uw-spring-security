/**
 * 
 */
package edu.wisc.uwss.local;

import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.UWUserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Interface providing a mechanism to bind a row from the local users properties
 * file to a {@link UWUserDetails} instance.
 *
 * This interface is used during application initialization - not during
 * authentication attempts. Since it is executed during Spring ApplicationContext initialization,
 * implementations should avoid injecting other service or dao interfaces, as it may be
 * affected by a race condition.
 *
 * If you have custom {@link UWUserDetails} that depend on services/daos to complete the model,
 * you may want to consider implementing a {@link LocalUsersAuthenticationAttemptCallback}; that interface
 * participates in the authentication attempt itself, not during application initialization.
 *
 * @deprecated use {@link LocalUserDetailsLoader} instead, to be removed in 2.0
 * @author Nicholas Blair
 */
@Deprecated
public interface LocalUserDetailsAttributesMapper {

  /**
   * Map a list of attributes to a {@link UWUserDetails}.
   * 
   * @param username the value to use from {@link UWUserDetails#getUsername()} 
   * @param row the values from a row in the properties file, in order
   * @return a {@link UWUserDetails} instance (never null)
   * @throws IllegalArgumentException if the row is malformed
   */
  UWUserDetails mapUser(String username, String [] row);

  /**
   * Default {@link LocalUserDetailsAttributesMapper} implementation.
   * 
   * @author Nicholas Blair
   */
  public static class Default implements LocalUserDetailsAttributesMapper {

    private static final Logger logger = LoggerFactory.getLogger(LocalUserDetailsAttributesMapper.class);

    /**
     * {@inheritDoc}
     * 
     * The format of the properties is as follows (key=value):
     * <pre>
     * username=password,fullName,firstName,lastName,emailAddress[,membership1,[membership2...]]
     * </pre>
     * 
     * Membership can be 1 or more UDDS values, comma separated.
     */
    @Override
    public UWUserDetails mapUser(String username, String[] values) {
      if (values.length > 6) {
        String[] uddsValues = Arrays.copyOfRange(values, 6, values.length);
        List<String> membership = new ArrayList<>();
        Collections.addAll(membership, uddsValues);
        // pvi, username, password, email,
        return new UWUserDetailsImpl(values[1], username, values[0], values[2], values[5], membership)
                .setFirstName(values[3])
                .setLastName(values[4]);
      } else if (values.length == 6) {
        // String pvi, String username, String password, String fullName, String emailAddress
        return new UWUserDetailsImpl(values[1], username, values[0], values[2], values[5])
                .setFirstName(values[3])
                .setLastName(values[4]);
      }
      return mapUserDeprecatedFormat(username, values);

    }

    @Deprecated
    UWUserDetails mapUserDeprecatedFormat(String username, String[] values) {
      if (values.length == 5) {
        logger.warn("Mapping deprecated in UWSS 1.2. Expected: username=password,fullName,firstName,lastName,emailAddress[,membership1,[membership2...]]");
        List<String> membership = new ArrayList<>();
        membership.add(values[4]);
        return new UWUserDetailsImpl(values[1], username, values[0], values[2], values[3], membership);
      } else {
        //length must equal 4
        logger.warn("Mapping deprecated in UWSS 1.2. Expected: username=password,fullName,firstName,lastName,emailAddress[,membership1,[membership2...]]");
        return new UWUserDetailsImpl(values[1], username, values[0], values[2], values[3]);
      }
    }
  }
}
