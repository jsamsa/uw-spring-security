/**
 * 
 */
package edu.wisc.uwss.local;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.util.Assert;

import edu.wisc.uwss.HasModifiableSource;
import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.UWUserDetailsImpl;

/**
 * {@link UserDetailsManager} implementation intended to provide local "demo" accounts.
 * This implementation is not intended for production use; it is primarily intended to support
 * development and/or test environments. If activating in a test environment, make sure that other
 * defense (firewall) is in place, as typically the demo users have weak, non-compliant passwords.
 * 
 * @author Collin Cudd
 */
public class LocalUserDetailsManagerImpl implements UserDetailsManager {
  
  private Logger logger = LoggerFactory.getLogger(this.getClass());
  private final Map<String, UWUserDetails> users = new ConcurrentHashMap<>();

  @Autowired(required=false)
  private List<LocalUsersAuthenticationAttemptCallback> callbacks = new ArrayList<>();
  @Autowired(required=false)
  private LocalUserDetailsAttributesMapper localUserDetailsAttributeMapper = new LocalUserDetailsAttributesMapper.Default();
  @Autowired(required=false)
  private LocalUserDetailsLoader localUserDetailsLoader = new LocalUserDetailsLoader.Default();
  @Value("#{environment['edu.wisc.uwss.local.userDetailsLoader.resource'] ?: 'classpath:/edu/wisc/uwss/local/local-users.json' }")
  private Resource localUserResource;
  @Value("#{environment['edu.wisc.uwss.local.userDetailsLoader.enabled'] ?: false }")
  private boolean loaderEnabled;

  private Properties properties = new Properties();

  /**
   * Visible for testing.
   *
   * @param loaderEnabled whether or not to enable the {@link LocalUserDetailsLoader}
   * @return a reference to this instance
   */
  LocalUserDetailsManagerImpl setLoaderEnabled(boolean loaderEnabled) {
    this.loaderEnabled = loaderEnabled;
    return this;
  }
  /**
   * Visible for testing.
   *
   * @param localUserResource the {@link Resource} to pass to the {@link LocalUserDetailsLoader}
   * @return a reference to this instance
   */
  LocalUserDetailsManagerImpl setLocalUserResource(Resource localUserResource) {
    this.localUserResource = localUserResource;
    return this;
  }
  /**
   *
   * @param localUserDetailsLoader
   * @return a reference to this instance
   */
  LocalUserDetailsManagerImpl setLocalUserDetailsLoader(LocalUserDetailsLoader localUserDetailsLoader) {
    this.localUserDetailsLoader = localUserDetailsLoader;
    return this;
  }

  /**
   * 
   */
  @PostConstruct
  public void init() {
    if(loaderEnabled) {
      logger.debug("LocalUserDetailsLoader of type {} enabled, processing resource {}", localUserDetailsLoader.getClass(), localUserResource);
      List<UWUserDetails> users = localUserDetailsLoader.loadUsers(localUserResource);
      for(UWUserDetails u : users) {
        addDemoUser(u);
      }
    } else {
      logger.debug("LocalUserDetailsAttributesMapper of type {} enabled, processing properties file of size {}", localUserDetailsAttributeMapper.getClass(), properties.size());
      for (Entry<Object, Object> entry : properties.entrySet()) {
        String[] values = ((String) entry.getValue()).split(",");
        String username = (String) entry.getKey();

        final UWUserDetails user = localUserDetailsAttributeMapper.mapUser(username, values);
        addDemoUser(user);
      }
    }
  }
  /**
   * Populate the internal map of {@link UWUserDetailsImpl} via the entries in the
   * injected {@link Properties}.
   * 
   * @see LocalUserDetailsAttributesMapper
   * @param properties
   */
  @Autowired
  public void setDemoUsers(@Qualifier("demo-users") Properties properties) {
   this.properties = properties;
  }

  /**
   * Visible for testing.
   *
   * @param callbacks
   */
  void setCallbacks(List<LocalUsersAuthenticationAttemptCallback> callbacks) {
    this.callbacks = callbacks;
  }

  /**
   * Add a user. Does nothing if the argument is null or the {@link UWUserDetailsImpl#getUsername()} is blank.
   * 
   * @param userDetails the user to add.
   */
  void addDemoUser(UWUserDetails userDetails) {
    if(userDetails != null && StringUtils.isNotBlank(userDetails.getUsername())) {
      users.put(userDetails.getUsername(), userDetails);
      if(userDetails instanceof HasModifiableSource) {
        ((HasModifiableSource) userDetails).setSource("edu.wisc.uwss.local-users");
      }
    }
  }
  
  void removeDemoUser(UWUserDetails userDetails) {
    if(userDetails != null && StringUtils.isNotBlank(userDetails.getUsername())) {
      users.remove(userDetails.getUsername());
    }
  }
  
  void removeDemoUser(String username) {
    if(StringUtils.isNotBlank(username)) {
      users.remove(username);
    }
  }
  /**
   * {@inheritDoc}
   * 
   * An important aspect of this method is the fact that a clone of the {@link UWUserDetailsImpl} stored
   * in the internal map is returned, NOT a reference to the instance in the map.
   * Spring Security nulls out the password field; if we were to return the instance in the map, we would
   * not be able to log in as that user again (as the password field is damaged).
   */
  @Override
  public UWUserDetails loadUserByUsername(String username)
      throws UsernameNotFoundException {
    UWUserDetails user = users.get(username);
    logger.debug("loadUserByUsername for {} returns {}", username, user);
    if(user == null) {
      throw new UsernameNotFoundException(username + " not found");
    }
    UWUserDetails result = SerializationUtils.clone(user);
    for(LocalUsersAuthenticationAttemptCallback callback: callbacks) {
      callback.success(result);
    }
    return result;
  }
  /**
   * @param user
   * @return {@link UWUserDetailsImpl}
   */
  protected UWUserDetails validateUserDetails(UserDetails user) {
    Assert.hasText(user.getUsername(), "Username may not be empty or null");
    Assert.isInstanceOf(UWUserDetails.class, user, "User must be an instance of UWUserDetails");
    return (UWUserDetails) user;
  }
  /* (non-Javadoc)
   * @see org.springframework.security.provisioning.UserDetailsManager#createUser(org.springframework.security.core.userdetails.UserDetails)
   */
  @Override
  public void createUser(UserDetails user) {
    UWUserDetails uwUser = validateUserDetails(user);
    Assert.isTrue(!userExists(uwUser.getUsername()), "Username already exists: "+user.getUsername());
    addDemoUser(uwUser);
  }
  /* (non-Javadoc)
   * @see org.springframework.security.provisioning.UserDetailsManager#updateUser(org.springframework.security.core.userdetails.UserDetails)
   */
  @Override
  public void updateUser(UserDetails user) { 
    UWUserDetails uwUser = validateUserDetails(user);
    Assert.isTrue(userExists(uwUser.getUsername()), "user doesn't exist: "+uwUser.getUsername());
    removeDemoUser(uwUser);
    addDemoUser(uwUser);
  }

  /* (non-Javadoc)
   * @see org.springframework.security.provisioning.UserDetailsManager#deleteUser(java.lang.String)
   */
  @Override
  public void deleteUser(String username) {
    Assert.isTrue(userExists(username), "Username does not exist: "+username);
    removeDemoUser(username);
  }

  /* (non-Javadoc)
   * @see org.springframework.security.provisioning.UserDetailsManager#changePassword(java.lang.String, java.lang.String)
   */
  @Override
  public void changePassword(String oldPassword, String newPassword) {
    throw new NotImplementedException("changePassword is not implemented");
  }

  /* (non-Javadoc)
   * @see org.springframework.security.provisioning.UserDetailsManager#userExists(java.lang.String)
   */
  @Override
  public boolean userExists(String username) {
    UWUserDetails user = null;
    try {
      user = loadUserByUsername(username);
    }catch(UsernameNotFoundException e) {}
    return user != null;
  }

  /**
   *
   * @return the number of {@link UWUserDetails} instances loaded in this instance
   */
  public int getUserCount() {
    return users.size();
  }

}
