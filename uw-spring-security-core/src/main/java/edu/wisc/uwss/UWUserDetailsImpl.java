/**
 * 
 */
package edu.wisc.uwss;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.util.DigestUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Implementation of {@link UWUserDetails} returned by the demo services in this package.
 * 
 * @author Nicholas Blair
 */
public class UWUserDetailsImpl extends User implements UWUserDetails, HasModifiableSource {

  /**
   * 
   */
  private static final long serialVersionUID = -8120331081953321708L;

  private String eppn;
  private final String pvi;
  private final String fullName;
  private final Collection<String> uddsMembership;
  private final String emailAddress;
  private final String emailAddressHash;
  private String source;
  private String customLogoutUrl;
  private String isisEmplid;
  private String firstName;
  private String lastName;
  /**
   * 
   * @param pvi
   * @param username
   * @param password
   * @param fullName
   * @param emailAddress
   */
  public UWUserDetailsImpl(String pvi, String username, String password,
      String fullName, String emailAddress) {
    this(pvi, username, password, fullName, emailAddress, Collections
        .<String> emptyList());
  }
  
  /**
   * The primary constructor for {@link UWUserDetails}.
   *
   * @param pvi
   * @param username
   * @param password
   * @param fullName
   * @param emailAddress
   * @param uddsMembership
   * @param authorities
   */
  public UWUserDetailsImpl(String pvi, String username, String password,
                           String fullName, String emailAddress,
                           Collection<String> uddsMembership, Collection<? extends GrantedAuthority> authorities) {
    super(username, password, authorities);
    this.pvi = pvi;
    this.fullName = fullName;
    this.uddsMembership = uddsMembership;
    this.emailAddress = StringUtils.isNotBlank(emailAddress) ?
            emailAddress.trim().toLowerCase() : emailAddress;
    this.emailAddressHash = StringUtils.isNotBlank(emailAddress) ? DigestUtils
            .md5DigestAsHex(this.emailAddress.getBytes()) : null;
  }

  /**
   * Utility factory method to help Jackson deserialize JSON objects to instances of this class.
   * There can be only one constructor or factory method on the class with {@link JsonCreator}
   * annotation.
   *
   * @param pvi
   * @param username
   * @param password
   * @param fullName
   * @param emailAddress
   * @param uddsMembership
   * @param authorities
   */
  @JsonCreator
  public static UWUserDetailsImpl newInstance(@JsonProperty("pvi") String pvi, @JsonProperty("username") String username, @JsonProperty("password") String password,
                           @JsonProperty("fullName") String fullName, @JsonProperty("emailAddress") String emailAddress,
                           @JsonProperty("uddsMembership") Collection<String> uddsMembership, @JsonProperty("authorities") Collection<String> authorities) {
    return new UWUserDetailsImpl(pvi, username, password, fullName, emailAddress,
            uddsMembership,
            AuthorityUtils.createAuthorityList(authorities.toArray(new String[]{})));
  }

  /**
   * @param pvi
   * @param username
   * @param password
   * @param fullName
   * @param uddsMembership
   */
  public UWUserDetailsImpl(String pvi, String username, String password,
       String fullName,  String emailAddress, Collection<String> uddsMembership) {
    this(pvi, username, password, fullName, emailAddress, uddsMembership, Collections.<GrantedAuthority>emptyList());
  }

  /**
   * Clone style constructor.
   * 
   * @param original
   */
  public UWUserDetailsImpl(UWUserDetails original) {
    this(original.getPvi(), original.getUsername(), original.getPassword(), original
        .getFullName(), original.getEmailAddress(), original
        .getUddsMembership(), original.getAuthorities());
    setCustomLogoutUrl(original.getCustomLogoutUrl());
    setEppn(original.getEppn());
    setIsisEmplid(original.getIsisEmplid());
    setSource(original.getSource());
    setFirstName(original.getFirstName());
    setLastName(original.getLastName());
  }

  /* (non-Javadoc)
   * @see edu.wisc.uwss.UWUserDetails#getPvi()
   */
  @Override
  public String getPvi() {
    return this.pvi;
  }

  public String getFullName() {
    return this.fullName;
  }

  public Collection<String> getUddsMembership() {
    return uddsMembership;
  }

  public String getSource() {
    return source;
  }
  /**
   * @param source the source to set
   */
  @Override
  public void setSource(String source) {
    this.source = source;
  }
  
  public String getCustomLogoutUrl() {
    return customLogoutUrl;
  }
  /**
   * @param customLogoutUrl the customLogoutUrl to set
   */
  public void setCustomLogoutUrl(String customLogoutUrl) {
    this.customLogoutUrl = customLogoutUrl;
  }

  public String getEmailAddress() {
    return this.emailAddress;
  }

  /**
   * Returns MD5 hash of {@link #getEmailAddress()} in Hex.
   * 
   * @return the MD5 hash of {@link #getEmailAddress()} if not blank,
   *         otherwise null
   */
  public final String getEmailAddressHash() {
    return this.emailAddressHash;
  }
  /**
   * @return the emplid
   */
  public String getIsisEmplid() {
    return isisEmplid;
  }
  /**
   * @param isisEmplid the ISIS emplid to set
   */
  public void setIsisEmplid(String isisEmplid) {
    this.isisEmplid = isisEmplid;
  }
  /**
   * {@inheritDoc}
   * 
   * Copied super's toString, but modified how {@link #getAuthorities()} is
   * represented.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("PVI: ").append(getPvi()).append("; ");
    sb.append("Username: ").append(getUsername()).append("; ");
    sb.append("Password: [PROTECTED]; ");
    sb.append("Enabled: ").append(isEnabled()).append("; ");
    sb.append("AccountNonExpired: ").append(isAccountNonExpired())
    .append("; ");
    sb.append("credentialsNonExpired: ").append(isCredentialsNonExpired())
    .append("; ");
    sb.append("AccountNonLocked: ").append(isAccountNonLocked())
    .append("; ");
    sb.append("Authorities: ")
    .append(Arrays.toString(getAuthorities().toArray()))
    .append("; ");
    sb.append("Source: ").append(getSource()).append("; ");
    sb.append("Emplid: ").append(getIsisEmplid()).append("; ");
    sb.append("Custom Logout Url: ").append(getCustomLogoutUrl());
    
    return sb.toString();
  }

  /**
   * @return the eppn
   */
  @Override
  public String getEppn() {
    return eppn;
  }

  /**
   * @param eppn the eppn to set
   */
  public void setEppn(String eppn) {
    this.eppn = eppn;
  }
  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }
  /**
   * @param firstName the firstName to set
   */
  public UWUserDetailsImpl setFirstName(String firstName) {
    this.firstName = firstName;
    return this;
  }
  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }
  /**
   * @param lastName the lastName to set
   */
  public UWUserDetailsImpl setLastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;

    UWUserDetailsImpl that = (UWUserDetailsImpl) o;

    if (eppn != null ? !eppn.equals(that.eppn) : that.eppn != null) return false;
    if (pvi != null ? !pvi.equals(that.pvi) : that.pvi != null) return false;
    if (fullName != null ? !fullName.equals(that.fullName) : that.fullName != null) return false;
    if (uddsMembership != null ? !uddsMembership.equals(that.uddsMembership) : that.uddsMembership != null)
      return false;
    if (emailAddress != null ? !emailAddress.equals(that.emailAddress) : that.emailAddress != null)
      return false;
    if (source != null ? !source.equals(that.source) : that.source != null) return false;
    if (customLogoutUrl != null ? !customLogoutUrl.equals(that.customLogoutUrl) : that.customLogoutUrl != null)
      return false;
    if (isisEmplid != null ? !isisEmplid.equals(that.isisEmplid) : that.isisEmplid != null)
      return false;
    if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null)
      return false;
    return lastName != null ? lastName.equals(that.lastName) : that.lastName == null;

  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + (eppn != null ? eppn.hashCode() : 0);
    result = 31 * result + (pvi != null ? pvi.hashCode() : 0);
    result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
    result = 31 * result + (uddsMembership != null ? uddsMembership.hashCode() : 0);
    result = 31 * result + (emailAddress != null ? emailAddress.hashCode() : 0);
    result = 31 * result + (source != null ? source.hashCode() : 0);
    result = 31 * result + (customLogoutUrl != null ? customLogoutUrl.hashCode() : 0);
    result = 31 * result + (isisEmplid != null ? isisEmplid.hashCode() : 0);
    result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
    result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
    return result;
  }
}
