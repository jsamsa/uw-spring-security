package edu.wisc.uwss.uds;

import edu.wisc.services.uds.person.v1_1.Identifiers;

/**
 * Factory interface for constructing {@link Identifiers} for use with {@link UdsPersonUserDetailsServiceImpl}.
 *
 * @author Nicholas Blair
 */
public interface IdentifiersFactory {

  /**
   *
   * @param value
   * @return an {@link Identifiers}
   */
  Identifiers withValue(String value);

  /**
   * {@link IdentifiersFactory} that returns "PVI" {@link Identifiers}.
   */
  public static class PVI implements IdentifiersFactory {
    @Override
    public Identifiers withValue(String value) {
      return new IdentifiersBuilder().withPvi(value).toIdentifiers();
    }
  }
  /**
   * {@link IdentifiersFactory} that returns "NetID" {@link Identifiers}.
   */
  public static class NetID implements IdentifiersFactory {
    @Override
    public Identifiers withValue(String value) {
      return new IdentifiersBuilder().withNetid(value).toIdentifiers();
    }
  }
  /**
   * {@link IdentifiersFactory} that returns PhotoID {@link Identifiers}.
   */
  public static class PhotoID implements IdentifiersFactory {
    @Override
    public Identifiers withValue(String value) {
      return new IdentifiersBuilder().withPhotoid(value).toIdentifiers();
    }
  }
}
