/**
 * 
 */
package edu.wisc.uwss.uds;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import edu.wisc.services.uds.person.v1_1.Identifier;
import edu.wisc.services.uds.person.v1_1.Identifiers;
import edu.wisc.services.uds.person.v1_1.Person;
import edu.wisc.uds.UdsPersonService;
import edu.wisc.uwss.UWUserDetails;

/**
 * Implementation of {@link UserDetailsService} backed by a {@link UdsPersonService} (required).
 *
 * An {@link IdentifiersFactory} can optionally be provided in order to control the "idName" and
 * "source" used for the {@link Identifiers} passed to the {@link UdsPersonService}.
 * The default {@link IdentifiersFactory} is to treat the value as a UW-Madison NetID.
 *
 * @see IdentifiersFactory
 * @author Collin Cudd
 */
public class UdsPersonUserDetailsServiceImpl implements UserDetailsService{
  
  protected UdsPersonService personService;
  protected IdentifiersFactory identifiersFactory = new IdentifiersFactory.NetID();

  @Autowired
  public UdsPersonUserDetailsServiceImpl(UdsPersonService personService) {
    this(personService, new IdentifiersFactory.NetID());
  }
  @Autowired(required=false)
  public UdsPersonUserDetailsServiceImpl(UdsPersonService personService, IdentifiersFactory identifiersFactory) {
    this.personService = personService;
    this.identifiersFactory = identifiersFactory;
  }
  @Override
  public UWUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Identifiers identifiers = identifiersFactory.withValue(username);
    Person person = personService.getPerson(identifiers);
    if(person == null) {
      throw new UsernameNotFoundException("no person found for " + username + " via UDS Person");
    }
    return new UdsPersonUserDetailsImpl(person);
  }
}
