/**
 * 
 */
package edu.wisc.uwss.uds;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

import edu.wisc.services.uds.person.v1_1.Appointment;
import edu.wisc.services.uds.person.v1_1.Appointments;
import edu.wisc.services.uds.person.v1_1.Demographic;
import edu.wisc.services.uds.person.v1_1.Employee;
import edu.wisc.services.uds.person.v1_1.Identifier;
import edu.wisc.services.uds.person.v1_1.Name;
import edu.wisc.services.uds.person.v1_1.Person;
import edu.wisc.uwss.UWUserDetails;

/**
 * {@link UWUserDetails} backed by a {@link Person}.
 * Most fields are computed fields, transforming equivalents from {@link Person}.
 * 
 * @author ctcudd
 */
public class UdsPersonUserDetailsImpl implements UWUserDetails {

  private static final long serialVersionUID = 2930942419916444606L;
  public static final String NETID_IDNAME = "NETID";
  public static final String PVI_IDNAME = "PVI";
  public static final String PHOTOID_IDNAME = "PHOTOID";
  public static final String UWMSNSUDS_SOURCE = "UWMSNSUDS";
  public static final String USER_DETAILS_SOURCE = "uds";
  
  private final Person person;
  
  /**
   * @param person must not be null
   */
  public UdsPersonUserDetailsImpl(Person person) {
    Assert.notNull(person, "Person argument must not be null");
    this.person = person;
  }
  /**
   * {@inheritDoc}
   * 
   * Always returns true.
   */
  @Override
  public boolean isAccountNonExpired() {
    return true;
  }
  /**
   * {@inheritDoc}
   * 
   * Always returns true.
   */
  @Override
  public boolean isAccountNonLocked() {
    return true;
  }
  /**
   * {@inheritDoc}
   * 
   * Always returns true.
   */
  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }
  /**
   * {@inheritDoc}
   * 
   * Always returns true.
   */
  @Override
  public boolean isEnabled() {
    return true;
  }
  /**
   * {@inheritDoc}
   * 
   * Always returns null, as users from this source can't log in.
   */
  @Override
  public String getCustomLogoutUrl() {
    return null;
  }


  /**
   * {@inheritDoc}
   * 
   * This implementation returns {@link Person#getDemographic()#getFullName()} or null.
   */
  @Override
  public String getFullName() {
    if(person.getDemographic() == null) return null;
    return person.getDemographic().getName().getFull();
  }
  
  /**
   * {@inheritDoc}
   * 
   * This implementation returns the UDDS values found within {@link Employee#getAppointments()}.
   */
  @Override
  public Collection<String> getUddsMembership() {
    Employee employee = person.getEmployee();
    if(employee != null) {
      Appointments appointments = employee.getAppointments();
      if(appointments != null) {
        Set<String> result = new HashSet<>();
        for(Appointment appointment: appointments.getAppointments()) {
          result.add(appointment.getUDDS().getCode());
        }
        return result;
      }
    }
    return Collections.emptyList();
  }

  /**
   * {@inheritDoc}
   * 
   * This implementation returns {@link #USER_DETAILS_SOURCE}
   */
  public String getSource() {
    return USER_DETAILS_SOURCE;
  }

  /**
   * {@inheritDoc}
   * 
   * This implementation returns {@link Person#getDemographic()#getEmailAddress()} or null.
   */
  public String getEmailAddress() {
    if(person.getDemographic() == null) return null;
    return person.getDemographic().getEmail();
  }

  /**
   * {@inheritDoc}
   * 
   * This implementation returns the netid username found within {@link Person#getIdentifiers()}.
   */
  @Override
  public String getUsername() {
    if(person.getIdentifiers() == null) {
      return null;
    }
    for(Identifier identifier : person.getIdentifiers().getIdentifiers()) {
      if(UWMSNSUDS_SOURCE.equals(identifier.getSource()) && NETID_IDNAME.equals(identifier.getIdName())) {
        return identifier.getValue();
      }
    }
    return null;
  }

  /**
   * {@inheritDoc}
   * 
   * This implementation always returns an empty list
   */
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return Collections.emptyList();
  }
  /**
   * {@inheritDoc}
   * 
   * This implementation always returns null.
   */
  @Override
  public String getPassword() {
    return null;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public String getPvi() {
    if(person.getIdentifiers() == null) {
      return null;
    }
    for(Identifier identifier: person.getIdentifiers().getIdentifiers()) {
      if(UWMSNSUDS_SOURCE.equals(identifier.getSource()) && PVI_IDNAME.equals(identifier.getIdName())) {
        return identifier.getValue();
      }
    }
    return null;
  }
  
  /**
   * {@inheritDoc}
   * 
   * This implementation always returns null; UDS Person does not return EPPN.
   */
  @Override
  public String getEppn() {
    return null;
  }
  /**
   * {@inheritDoc}
   * 
   * This implementation always returns null.
   * 
   * TODO it may be possible to extract an emplid value from person.getIdentifiers
   * Example observed: edu.wisc.services.uds.person.v1_1.Identifier@715ef617[source=UWMSNSS, idName=EMPLID, value=000123456]
   */
  @Override
  public String getIsisEmplid() {
    return null;
  }

  @Override
  public String toString() {
    return "UdsPersonUserDetailsImpl{" +
            "person=" + person +
            '}';
  }

  /**
   * {@inheritDoc} 
   */
  @Override
  public String getFirstName() {
    return getName() != null ? getName().getFirst() : null;
  }
  /**
   * {@inheritDoc}
   */
  @Override
  public String getLastName() {
    return getName() != null ? getName().getLast() : null;
  }
  /**
   * 
   * @return {@link Demographic#getName()}, or null if not available
   */
  protected Name getName() {
    if(person.getDemographic() == null) {
      return null;
    }
    return person.getDemographic().getName();
  }
}
