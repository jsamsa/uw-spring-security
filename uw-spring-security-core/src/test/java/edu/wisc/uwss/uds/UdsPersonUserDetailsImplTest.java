/**
 * 
 */
package edu.wisc.uwss.uds;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Test;

import edu.wisc.services.uds.person.v1_1.Demographic;
import edu.wisc.services.uds.person.v1_1.Identifier;
import edu.wisc.services.uds.person.v1_1.Identifiers;
import edu.wisc.services.uds.person.v1_1.Name;
import edu.wisc.services.uds.person.v1_1.Person;

/**
 * Tests for {@link UdsPersonUserDetailsImpl}
 * @author Collin Cudd
 */
public class UdsPersonUserDetailsImplTest {

  /**
   * Test verifies an {@link IllegalArgumentException} is thrown if you attempt to construct a
   * {@link UdsPersonUserDetailsImpl} with a null {@link Person}.
   */
  @Test(expected=IllegalArgumentException.class)
  public void nullPerson_throwsIllegalArgumentException() {
    new UdsPersonUserDetailsImpl(null);
  }
  
  /**
   * Test verifies behaviour when an 'empty' {@link Person} (no properties set) is used to construct
   * a {@link UdsPersonUserDetailsImpl}.
   */
  @Test
  public void emptyPerson() {
    Person person = new Person();
    UdsPersonUserDetailsImpl userDetails = new UdsPersonUserDetailsImpl(person);
    assertNotNull(userDetails);
    assertEquals(Collections.emptyList(), userDetails.getAuthorities());
    assertNull(userDetails.getCustomLogoutUrl());
    assertNull(userDetails.getEmailAddress());
    assertNull(userDetails.getFullName());
    assertNull(userDetails.getPassword());
    assertNull(userDetails.getPvi());
    assertEquals(UdsPersonUserDetailsImpl.USER_DETAILS_SOURCE, userDetails.getSource());
    assertEquals(Collections.emptyList(), userDetails.getUddsMembership());
    assertNull(userDetails.getUsername());

    assertNull(userDetails.getFirstName());
    assertNull(userDetails.getLastName());
  }
  
  /**
   * Verify {@link UdsPersonUserDetailsImpl#getEmailAddress()}.
   */
  @Test
  public void getEmail_control() {
    Person person = new Person();
    Demographic demographic = new Demographic();
    demographic.setEmail("bbadger@wisc.edu");
    person.setDemographic(demographic);
    assertEquals("bbadger@wisc.edu", new UdsPersonUserDetailsImpl(person).getEmailAddress());
  }
  

  /**
   * Successful test returning expected {@link UdsPersonUserDetailsImpl#getUsername()}.
   */
  @Test
  public void getUsername_control() {
    String netid = "bbadger";
    Identifier identifier = new Identifier();
    identifier.setSource("UWMSNSUDS");
    identifier.setIdName("NETID");
    identifier.setValue(netid);
    
    Identifiers identifiers = new Identifiers();
    identifiers.getIdentifiers().add(identifier);
    Person person = new Person();
    
    person.setIdentifiers(identifiers);
    assertEquals(netid, new UdsPersonUserDetailsImpl(person).getUsername());
  }
  /**
   * Confirm null result for {@link UdsPersonUserDetailsImpl#getUsername()}
   * when photoId not present
   */
  @Test
  public void getUsername_missing() {
    Person person = new Person();
    assertNull(new UdsPersonUserDetailsImpl(person).getUsername());
  }
  /**
   * Confirm successfully retrieve {@link UdsPersonUserDetailsImpl#getFirstName()}.
   */
  @Test
  public void getFirstName_control() {
    Person person = new Person();
    Demographic demographic = new Demographic();
    Name name = new Name();
    name.setFirst("Bucky");
    demographic.setName(name);
    person.setDemographic(demographic);
    assertEquals("Bucky", new UdsPersonUserDetailsImpl(person).getFirstName());
  }
  /**
   * Confirm successfully retrieve {@link UdsPersonUserDetailsImpl#getLastName()}.
   */
  @Test
  public void getLastName_control() {
    Person person = new Person();
    Demographic demographic = new Demographic();
    Name name = new Name();
    name.setLast("Badger");
    demographic.setName(name);
    person.setDemographic(demographic);
    assertEquals("Badger", new UdsPersonUserDetailsImpl(person).getLastName());
  }

  /**
   * Confirm successful result for {@link UdsPersonUserDetailsImpl#getPvi()}.
   */
  @Test
  public void getPvi_success() {
    String pvi = "UW111E111";
    Identifier identifier = new Identifier();
    identifier.setSource("UWMSNSUDS");
    identifier.setIdName("PVI");
    identifier.setValue(pvi);

    Identifiers identifiers = new Identifiers();
    identifiers.getIdentifiers().add(identifier);
    Person person = new Person();
    person.setIdentifiers(identifiers);

    assertEquals(pvi, new UdsPersonUserDetailsImpl(person).getPvi());
  }

}
