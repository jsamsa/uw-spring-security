/**
 * 
 */
package edu.wisc.uwss.preauth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.preauth.PreauthenticatedUserDetailsAttributeMapper.Default;

/**
 * Tests for {@link Default}.
 * 
 * @author Nicholas Blair
 */
public class PreauthenticatedUserDetailsAttributeMapperTest {

  private PreauthenticatedUserDetailsAttributeMapper.Default filter = new PreauthenticatedUserDetailsAttributeMapper.Default();

  /**
   * Verify expected behaviour for {@link PreauthenticatedUserDetailsAttributeMapper#mapUser(HttpServletRequest)}
   * for a simple case with 1 value for each expected header.
   */
  @Test
  public void mapUser_success() {
    MockHttpServletRequest request = mockRequest();
    request.addHeader("wisceduudds", "udds1234");
    request.addHeader("isMemberOf", "uw:domain:something");
    
    UWUserDetails result = filter.mapUser(request);
    assertNotNull(result);
    assertMockAttributes(result);

    assertEquals(Collections.singletonList("udds1234"), result.getUddsMembership());
    assertEquals(1,result.getAuthorities().size());
    assertTrue(result.getAuthorities().contains(new SimpleGrantedAuthority("uw:domain:something")));
  }

  /**
   * Verify expected behaviour for {@link PreauthenticatedUserDetailsAttributeMapper#mapUser(HttpServletRequest)}
   * when the isMemberOf header is empty.
   */
  @Test
  public void mapUser_empty_isMemberOf() {
    MockHttpServletRequest request = mockRequest();

    UWUserDetails result = filter.mapUser(request);
    assertNotNull(result);
    assertMockAttributes(result);
    assertEquals(0, result.getAuthorities().size());
  }
  /**
   * Verify expected behaviour for {@link PreauthenticatedUserDetailsAttributeMapper#mapUser(HttpServletRequest)}
   * when the isMemberOf contains blank values.
   */
  @Test
  public void mapUser_isMemberOf_contains_blank_values() {
    MockHttpServletRequest request = mockRequest();
    request.addHeader("isMemberOf", ";    ");

    UWUserDetails result = filter.mapUser(request);
    assertNotNull(result);
    assertMockAttributes(result);
    assertEquals(0, result.getAuthorities().size());
  }
  /**
   * Verify expected behaviour for {@link PreauthenticatedUserDetailsAttributeMapper#mapUser(HttpServletRequest)}
   * when the isMemberOf header has multiple values.
   */
  @Test
  public void mapUser_multipleManifestGroups() {
    MockHttpServletRequest request = mockRequest();
    request.addHeader("isMemberOf", "uw:domain:onegroup;uw:domain:anothergroup");

    UWUserDetails result = filter.mapUser(request);
    assertNotNull(result);
    assertMockAttributes(result);

    assertEquals(2,result.getAuthorities().size());
    assertTrue(result.getAuthorities().contains(new SimpleGrantedAuthority("uw:domain:onegroup")));
    assertTrue(result.getAuthorities().contains(new SimpleGrantedAuthority("uw:domain:anothergroup")));
  }
  /**
   * Verify expected behaviour for {@link PreauthenticatedUserDetailsAttributeMapper#mapUser(HttpServletRequest)}
   * when the isMemberOf header has multiple values.
   */
  @Test
  public void mapUser_multipleudds() {
    MockHttpServletRequest request = mockRequest();
    request.addHeader("wisceduudds", "A061234");
    request.addHeader("wisceduudds", "B062345");

    UWUserDetails result = filter.mapUser(request);
    assertNotNull(result);
    assertMockAttributes(result);

    assertEquals(2,result.getUddsMembership().size());
    assertTrue(result.getUddsMembership().contains("A061234"));
    assertTrue(result.getUddsMembership().contains("B062345"));
  }
  /**
   * Verify behavior of {@link Default#toCustomLogoutUrl(String)} for
   * null input.
   */
  @Test
  public void toCustomLogoutUrl_null() {
    assertNull(filter.toCustomLogoutUrl(null));
  }
  /**
   * Verify behavior of {@link Default#toCustomLogoutUrl(String)} for
   * empty input.
   */
  @Test
  public void toCustomLogoutUrl_empty() {
    assertNull(filter.toCustomLogoutUrl(""));
  }
  /**
   * Verify behavior of {@link Default#toCustomLogoutUrl(String)} for
   * invalid input.
   */
  @Test
  public void toCustomLogoutUrl_invalid() {
    assertNull(filter.toCustomLogoutUrl("foo"));
  }
  /**
   * Verify behavior of {@link Default#toCustomLogoutUrl(String)} for
   * a valid input with no {@link URL#getPath()}.
   */
  @Test
  public void toCustomLogoutUrl_no_path() {
    assertEquals("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/logout/", filter.toCustomLogoutUrl("https://somewhere.wisc.edu"));
  }
  /**
   * Verify behavior of {@link Default#toCustomLogoutUrl(String)} for
   * expected valid input.
   */
  @Test
  public void toCustomLogoutUrl_success() {
    assertEquals("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/logout/", filter.toCustomLogoutUrl("https://somewhere.wisc.edu/idp/shibboleth"));
  }
  /**
   * Verify behavior of {@link Default#toCustomLogoutUrl(String)} for
   * expected valid input and a different customLogoutPrefix.
   */
  @Test
  public void toCustomLogoutUrl_alternate_prefix() {
    filter.setCustomLogoutPrefix("/Chibbolath.sso/Logout?return=");
    assertEquals("/Chibbolath.sso/Logout?return=https://somewhere.wisc.edu/logout/", filter.toCustomLogoutUrl("https://somewhere.wisc.edu/idp/shibboleth"));
  }
  /**
   * Verify behavior of {@link Default#toCustomLogoutUrl(String)} for
   * expected valid input and a different customLogoutSuffix.
   */
  @Test
  public void toCustomLogoutUrl_alternate_suffix() {
    filter.setCustomLogoutSuffix("/luguot/");
    assertEquals("/Shibboleth.sso/Logout?return=https://somewhere.wisc.edu/luguot/", filter.toCustomLogoutUrl("https://somewhere.wisc.edu/idp/shibboleth"));
  }

  /**
   *
   * @return a suitable {@link MockHttpServletRequest} for our tests
   */
  protected MockHttpServletRequest mockRequest() {
    MockHttpServletRequest request = new MockHttpServletRequest();

    request.addHeader("eppn", "somebody@wisc.edu");
    request.addHeader("wiscedupvi", "1234567");
    request.addHeader("uid", "somebody");
    request.addHeader("cn", "some body");
    request.addHeader("mail", "some.body@wisc.edu");
    request.addHeader("wisceduisisemplid", "0000123456");
    request.addHeader("Shib-Identity-Provider", "https://logintest.wisc.edu/idp/shibboleth");
    return request;
  }

  /**
   *
   * @param result confirm expected values set by {@link #mockRequest()}
   */
  protected void assertMockAttributes(UWUserDetails result) {
    assertEquals("somebody", result.getUsername());
    assertEquals("somebody@wisc.edu", result.getEppn());
    assertEquals("1234567", result.getPvi());
    assertEquals("some body", result.getFullName());
    assertEquals("some.body@wisc.edu", result.getEmailAddress());
    assertEquals("0000123456", result.getIsisEmplid());
    assertEquals("/Shibboleth.sso/Logout?return=https://logintest.wisc.edu/logout/", result.getCustomLogoutUrl());
  }
}
