## uw-spring-security

This project is intended to provide a re-usable library that integrates [Spring Security](http://projects.spring.io/spring-security/) with UW's common
authentication approach. 

The core of the project contains a re-usable extension of Spring's [UserDetails](http://docs.spring.io/spring-security/site/docs/3.2.6.RELEASE/apidocs/org/springframework/security/core/userdetails/UserDetails.html) called [UWUserDetails](uw-spring-security-core/src/main/java/edu/wisc/uwss/UWUserDetails.java). This interface (and implementations) provide
commonly used user attributes, like PVI, UDDS membership, full name, and email address. 

One of the biggest benefits of this project is that the core data model is re-used between:

* a production-ready configuration (using Shibboleth), and 
* a local development environment configuration *that does not require developers to install a Shibboleth service provider*.

[Detailed usage documentation can be found at the project wiki.](https://git.doit.wisc.edu/adi-ia/uw-spring-security/wikis/home).

This project was also recently the focus of an [Application Design Review Brown Bag](https://wiki.doit.wisc.edu/confluence/display/SOACOE/2016-03-04+Agenda).

### Why this?

[Spring Security](http://projects.spring.io/spring-security/) is an incredibly powerful and amazing way to secure your web application. There is arguably no better tool for Spring applications.

[Spring Security](http://projects.spring.io/spring-security/) however is terribly complex to integrate in a project. [Read through the 'Hello World' example in Spring Security's reference docs](http://docs.spring.io/spring-security/site/docs/4.0.3.RELEASE/reference/htmlsingle/#jc).
It's a long read, and when you are done *you will be no closer to having Spring Security configured in a manner that's appropriate for use in a UW application*.

Enter uw-spring-security. To get Spring Security fully integrated in your project, for both local development **and** deployed instances behind a Shibboleth Service Provider:

```java
@Configuration
@Import(EverythingRequiresAuthenticationConfiguration.class)
public class MyApplicationConfiguration {
}
```

That's it! Your application will now be able to call Spring Security's:

```java
SecurityContextHolder.getContext().getAuthentication().getPrincipal()
```

and get back an instance of [UWUserDetails](uw-spring-security-core/src/main/java/edu/wisc/uwss/UWUserDetails.java), which looks like this:

```
{
  pvi: "UW000A000",
  username: "admin",
  password: null,
  fullName: "Amy Administrator",
  emailAddress: "amy.administrator@demo.wisc.edu",
  uddsMembership: [
    "A535900"
  ],
  authorities: [ ],
  accountNonExpired: true,
  accountNonLocked: true,
  credentialsNonExpired: true,
  enabled: true,
  eppn: null,
  emailAddressHash: "b09ed4fa2272feede8b472d1184829dd",
  source: "local-users",
  customLogoutUrl: null,
  isisEmplid: null,
  firstName: null,
  lastName: null
}
```


### Adding the dependencies to your project

The following instructions assuming that you have access to the UW Releases repository in our [Maven Repository Manager](https://wiki.doit.wisc.edu/confluence/display/ST/Maven+Repository+Manager).

```xml
  <repositories>
    <repository>
      <id>code.doit-uw-releases</id>
      <url>https://code.doit.wisc.edu/maven/content/repositories/uw-releases/</url>
    </repository>
  </repositories>
```

Add the following dependencies:

```xml
  <dependency>
    <groupId>edu.wisc.uwss</groupId>
    <artifactId>uw-spring-security-core</artifactId>
  </dependency>
  <dependency>
    <groupId>edu.wisc.uwss</groupId>
    <artifactId>uw-spring-security-config</artifactId>
  </dependency>
```
    
The former, -core, should be a dependency in modules that integrate the user model within your service tier. The latter, -config, depends on -core and should be a dependency of your web application.  
    
The uw-spring-security-sample-war is an example of how to activate the Spring `@Configuration` classes provided by the -config module in your application. Look at the *edu.wisc.uwss.sample.configuration* package for more detail. 

### Release Management

This project follows [Semantic Versioning](http://semver.org). Releases are published to the 'UW Releases' Repository in the [Shared Tools Maven Artifact Repository](https://wiki.doit.wisc.edu/confluence/display/ST/Maven+Repository+Manager)

Add the following repository to your Maven/Gradle build file:

```xml
  <repositories>
    <repository>
      <id>code.doit-uw-releases</id>
      <url>https://code.doit.wisc.edu/maven/content/repositories/uw-releases/</url>
    </repository>
  </repositories>
```